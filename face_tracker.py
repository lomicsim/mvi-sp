import math
import face_recognition as fr


class BoundingBox:
    def __init__(self, trbl=None, lt=None, rb=None):
        if trbl is not None:
            t, r, b, l = trbl
            self.p1 = (l, t)
            self.p2 = (r, b)
        else:
            self.p1 = lt
            self.p2 = rb

    def thumb(self, image):
        x1, y1 = self.p1
        x2, y2 = self.p2
        return image[y1:y2, x1:x2]

    def get_w(self):
        return self.p2[0] - self.p1[0]

    def get_h(self):
        return self.p2[1] - self.p1[1]

    def compare(self, bb):
        w = max(self.p2[0], bb.p2[0]) - min(self.p1[0], bb.p1[0])
        h = max(self.p2[1], bb.p2[1]) - min(self.p1[1], bb.p1[1])
        return 1.0 * w * h / max(self.get_w() * self.get_h(), bb.get_w() * bb.get_h())


class Hist:
    def __init__(self, face, num_bins):
        self.data = [[0] * num_bins, [0] * num_bins, [0] * num_bins]
        self.num_bins = num_bins
        for i in range(len(face)):
            for j in range(len(face[0])):
                for c in range(3):
                    self.data[c][math.floor(face[i][j][c] / 256.0 * num_bins)] += 1

    def compare(self, hist):
        ret = []
        for c in range(3):
            min_sum = 0.0
            sum_all = 1.0
            for h in range(self.num_bins):
                min_sum += min(self.data[c][h], hist.data[c][h])
                sum_all += self.data[c][h] + hist.data[c][h]
            ret.append(min_sum / sum_all)
        return 1 - sum(ret) / 3.0


class Face:
    def __init__(self, image, bb, num_bins):
        self.img = image
        self.hist = Hist(bb.thumb(image), num_bins)
        self.bb = bb

    def calc_hist_metric(self, face):
        return self.hist.compare(face.hist)

    def calc_bb_metric(self, face):
        return self.bb.compare(face.bb)

    def compare(self, face, bb_threshold, hist_threshold):
        bb = self.calc_bb_metric(face)
        hist = face.calc_hist_metric(face)
        if bb <= bb_threshold and hist <= hist_threshold:
            return bb + hist
        else:
            return math.inf


class Track:
    def __init__(self, face, frame_num):
        self.faces = [(frame_num, face)]
        self.first_frame = self.last_frame = frame_num
        self.active = True
        self.person = None

    def merge(self, track):
        self.faces.append((track.last_frame, track.face()))
        self.last_frame = track.last_frame

    def face(self):
        return self.faces[0][1]

    def face_time(self):
        return self.faces[0][0]

    def pop_face(self):
        self.faces.pop(0)


class TrackingWindow:
    def __init__(self, length_in_frames, bb_threshold, hist_threshold):
        self.length = length_in_frames
        self.active_tracks = []
        self.bb_threshold = bb_threshold
        self.hist_threshold = hist_threshold

    def _find_closest_track(self, track):
        best_dist = math.inf
        best = None
        for t in self.active_tracks:
            cur = track.face().compare(t.face(), self.bb_threshold, self.hist_threshold)
            if cur < best_dist:
                best_dist = cur
                best = t
        return best

    def add(self, track):
        t = self._find_closest_track(track)
        if t is None:
            self.active_tracks.append(track)
            return True
        else:
            t.merge(track)
            return False

    def oldest_active_frame_num(self):
        if len(self.active_tracks) == 0:
            return math.inf
        return min(map(lambda t: t.first_frame, self.active_tracks))

    def update(self, cur_frame):
        act = []
        for t in self.active_tracks:
            if cur_frame - t.last_frame > self.length:  # too old
                t.active = False
            else:
                act.append(t)
        self.active_tracks = act


class FrameInfo:
    def __init__(self, num, image):
        self.num = num
        self.image = image
        self.tracks_started_here = []

    def add_track(self, tr):
        self.tracks_started_here.append(tr)


def detect_faces(image, detector='hog'):
    faces = fr.face_locations(image, model=detector)
    ret = []
    for face in faces:
        ret.append(BoundingBox(face))
    return ret
