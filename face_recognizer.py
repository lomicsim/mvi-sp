import argparse
import math
import os
import sys
from os.path import join
from random import randint
from timeit import default_timer as timer

import cv2
import face_recognition as fr
import numpy as np
import tensorflow as tf

from MovieMetadataDownloader import MovieMetadataDownloader
from face_tracker import TrackingWindow, detect_faces, FrameInfo, Track, Face
from recognize.Recognizer import Recognizer

FRAMES_DIR = "frames/"
VIDEO_WIDTH = 900
HIST_BINS = 30
TRACKING_WINDOW_LEN_STEPS = 6
TRACKING_BB_THRESHOLD = 1.5
TRACKING_HIST_THRESHOLD = 0.75


def draw_face(image, bb, person, col):
    cv2.rectangle(image, bb.p1, bb.p2, col, 3)
    cv2.putText(image, person.name, (bb.p1[0], bb.p2[1]), cv2.FONT_ITALIC, 0.3, (0, 0, 0), 1, cv2.LINE_AA)


def resize_image(image, target_h, target_w):
    return cv2.resize(image, (int(target_w), int(target_h)), cv2.INTER_NEAREST)


def get_video_dim(cap):
    status, image = cap.read()
    assert (status is True)
    return len(image), len(image[0])


def processVideoFile(args, video_name, read_frequency, recognizer, movie):
    cap = cv2.VideoCapture(video_name)
    fps = cap.get(cv2.CAP_PROP_FPS)
    step = max(int(fps * read_frequency), 1)
    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    h, w = get_video_dim(cap)
    h = min(int(VIDEO_WIDTH * h / w), h)
    w = min(VIDEO_WIDTH, w)

    print("FPS={} step={} len={} num={}".format(fps, step, length, math.floor(length / step)))

    # fourcc = cv2.VideoWriter_fourcc(*'XVID')
    # out = cv2.VideoWriter('output.avi', fourcc, 20.0, (w, h))

    active_frames = []
    window = TrackingWindow(TRACKING_WINDOW_LEN_STEPS * step, TRACKING_BB_THRESHOLD, TRACKING_HIST_THRESHOLD)

    start = timer()
    for frame_no in range(args.start, length, step):
        tm = timer()

        # Capture frame & resize it
        cap.set(cv2.CAP_PROP_POS_FRAMES, frame_no)  # seek frame
        status, image = cap.read()
        assert (status is True)
        image = resize_image(image, h, w)

        # Detect & collect faces in current frame
        faces = detect_faces(image, detector=args.detector)
        frame_info = FrameInfo(frame_no, image)
        window.update(frame_no)
        for bb in faces:
            tr = Track(Face(image, bb, HIST_BINS), frame_no)
            if window.add(tr):
                frame_info.add_track(tr)
        active_frames.append(frame_info)

        # Process all frames with already inactive tracks
        eldest_active_frame = window.oldest_active_frame_num()
        tracks_to_process = []
        while len(active_frames) > 0 and active_frames[0].num < eldest_active_frame:
            cur_frame = active_frames[0]

            # Add tracks starting in this frame (we know that they aren't active)
            for finished_track in cur_frame.tracks_started_here:
                recognize_track(args, recognizer, movie, finished_track)
                tracks_to_process.append(finished_track)

            # NOW each track in tracks_to_process is active for this frame and has been finished analysing
            for t in tracks_to_process:
                assert (t.active is False)
                if t.person is not None and cur_frame.num <= t.last_frame:
                    draw_face(cur_frame.image, t.face().bb, t.person, t.color)
                    if t.face_time() <= cur_frame.num:  # TODO: here we could interpolate between frames
                        t.pop_face()

            # NOW it is ok to finish analysing this frame and throw it away
            cv2.imwrite(join(FRAMES_DIR, "frame_%d.jpg" % cur_frame.num), cur_frame.image)
            active_frames.pop(0)

        print("{}: {}s active_frames: {} active_tracks: {} oldest: {}".format(frame_no, timer() - tm,
                                                                              len(active_frames),
                                                                              len(window.active_tracks),
                                                                              eldest_active_frame))
        #     out.write(image)

    cap.release()
    # out.release()
    print("time={} processed frames={} time per frame: {}".format(timer() - start, length / step,
                                                                  (timer() - start) / (length / step)))


def recognize_movie_cast(args, movie, recognizer):
    print("recognizing movie cast of size {}...".format(len(movie.cast)))
    if args.recognizer == 'fr':
        movie.embeddings = []
        movie.embedding_persons = []
    for person in movie.cast:
        img_path = person.headshot
        img = cv2.imread(img_path)
        faces = detect_faces(img)
        if len(faces) > 0:
            img = faces[0].thumb(img)
            if args.recognizer == 'cnn':
                img = cv2.resize(img, (args.image_width, args.image_height), cv2.INTER_NEAREST)
                person.img = img
                person.embedding = recognizer.get(np.array([img]))[0]
            else:
                encodings = fr.face_encodings(img)
                if len(encodings) > 0:
                    movie.embeddings.append(encodings[0])
                    movie.embedding_persons.append(person)
    if args.recognizer == 'fr':
        movie.embeddings = np.array(movie.embeddings)


def recognize_track(args, recognizer, movie, track):
    track.color = (randint(0, 256), randint(0, 256), randint(0, 256))
    best = math.inf
    best_person = None
    if args.recognizer == 'cnn':
        faces = np.array(
            [cv2.resize(face[1].img, (recognizer.image_width, recognizer.image_height), cv2.INTER_NEAREST) for face in
             track.faces])
        for embeddings in recognizer.get(faces):
            for p in movie.cast:
                if p.embedding is None:
                    continue
                diff = np.subtract(embeddings, p.embedding)
                dist = np.sum(np.square(diff))
                if dist < best:
                    best = dist
                    best_person = p
    else:
        score = [0] * len(movie.embeddings)
        for face in track.faces:
            enc = fr.face_encodings(face[1].img)
            if len(enc) > 0:
                score = fr.face_distance(movie.embeddings, enc[0])
        for i in range(len(movie.embeddings)):
            if score[i] < 0.6 and best > score[i]:
                best = score[i]
                best_person = movie.embedding_persons[i]

    track.person = best_person


def load_movie(args, movie_name, recognizer):
    print("loading movie meta-data for '{}'...".format(movie_name))
    movie_dwn = MovieMetadataDownloader(args)
    movie = movie_dwn.get_movie(movie_name)
    recognize_movie_cast(args, movie, recognizer)
    return movie


def main(args):
    with tf.Session() as sess:
        recognizer = Recognizer(sess, args.model_path, args.embedding_size, args.image_width, args.image_height)

        movie = load_movie(args, args.movie, recognizer)
        processVideoFile(args, args.filename, args.freq, recognizer, movie)


def parse_arguments(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument('-f', '--filename', type=str, help='Video file',
                        default='/Users/simonlomic/Downloads/shutterIsland/5iaYLCiq5RM.mp4')
    parser.add_argument('-m', '--movie', type=str, help='Movie name',
                        default='Shutter Island')
    parser.add_argument('--freq', type=float, help='Frequency of reading.', default=.1)
    parser.add_argument('--start', type=int, help='First frame', default=1)
    parser.add_argument('--detector', choices=['hog', 'cnn'], help='Hog is fast but dumb, cnn is slow but clever.',
                        default='cnn')
    parser.add_argument('--image_width', type=int, help='Image width', default=112)
    parser.add_argument('--image_height', type=int, help='Image height', default=112)
    parser.add_argument('--embedding_size', type=int, help='Embedding size', default=112)
    parser.add_argument('--model_path', type=str, help='Path to the model', default='recognize/model/trained_model')
    parser.add_argument('--movie_dir', type=str, help='Path to the movie pickles', default='movies')
    parser.add_argument('--person_dir', type=str, help='Path to the person headshots', default='persons')
    parser.add_argument('--max_movies_searched', type=int, help='Max num of searched movies', default=10)
    parser.add_argument('--max_cast_size', type=int, help='Maximal size of movie\'s cast', default=40)
    parser.add_argument('--recognizer', type=str, choices=['fr', 'cnn'], default='fr')

    return parser.parse_args(argv)


if __name__ == '__main__':
    os.system("rm frames/*")
    main(parse_arguments(sys.argv[1:]))
