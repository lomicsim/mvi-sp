
# Face recognition ve filmech

![frame](frame.jpg)

S použitím detektoru tváří a datasetu celebrit (CelebA, MS-Celeb-1M) detekujte, v jakých částech filmů se každý herec objevuje. Vstupem bude film a seznam herců, kteří v něm figurují. Výsledkem by měla být např. informace, že Brad Pitt hraje převážně v druhé části filmu Fight Club a celkově je na plátně 50 minut.
Pro inspiraci: Automatic Face Recognition for Film Character Retrieval in Feature-Length Films - O. Arandjelovic et al. (2005).

## Data

CNN je připravená k trénování na CASIA Webface datasetu a testování na LFW. Pro lepší výsledky je vhodné dataset předzpracovat libovolným silným rekognizátorem obličejů na velikost 112x112.

## Spuštění

``python face_recognizer --help``

