import os
import time
from datetime import datetime

import tensorflow as tf
from tensorflow.contrib import slim

import cnn
import loss
from data import webface

EPOCH_SIZE = 30
MAX_NUM_OF_EPOCHS = 100
RATE_DECAY_EPOCH_INTERVAL = 100
EMBEDDING_SIZE = 112
LEARNING_RATE = 0.05

subdir = datetime.strftime(datetime.now(), '%Y%m%d-%H%M%S')
log_dir = os.path.join(os.path.expanduser("logs/"), subdir)

if not os.path.isdir(log_dir):  # Create the log directory if it doesn't exist
    os.makedirs(log_dir)
model_dir = os.path.join(os.path.expanduser("model/"), subdir)

global_step = tf.Variable(0, trainable=False, name='global_step')
learning_rate_placeholder = tf.placeholder(tf.float32, name='learning_rate')
phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')

image_it, label_it, it, dataset_size = webface.get_dataset(EPOCH_SIZE, MAX_NUM_OF_EPOCHS)

learning_rate = tf.train.exponential_decay(learning_rate_placeholder, global_step,
                                           RATE_DECAY_EPOCH_INTERVAL * EPOCH_SIZE, 1)
# Summaries
tf.summary.scalar('learning_rate', learning_rate)
summary_op = tf.summary.merge_all()

# Optimizer
opt = tf.train.AdamOptimizer(learning_rate, beta1=0.9, beta2=0.999, epsilon=0.1)

# Construct network
network = cnn.cnn(image_it, EMBEDDING_SIZE)
network = slim.batch_norm(network, is_training=True, decay=0.997, epsilon=1e-5, scale=True,
                          updates_collections=tf.GraphKeys.UPDATE_OPS)
cos_loss = loss.cos_loss(network, label_it, dataset_size)
reg_loss = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
loss = tf.reduce_mean(cos_loss + reg_loss)
reg_loss = tf.reduce_mean(reg_loss)

# Calc Gradients
grads = opt.compute_gradients(loss, tf.trainable_variables(), colocate_gradients_with_ops=True)
apply_gradient_op = opt.apply_gradients(grads, global_step=global_step)
update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
with tf.control_dependencies(update_ops):
    train_op = tf.group(apply_gradient_op)

# Init session and coordinator
sess = tf.Session()
sess.run(tf.global_variables_initializer(), feed_dict={phase_train_placeholder: True})
sess.run(tf.local_variables_initializer(), feed_dict={phase_train_placeholder: True})
sess.run(it.initializer)
save_vars = [var for var in tf.global_variables() if 'global_step' not in var.name]
saver = tf.train.Saver(save_vars, max_to_keep=3)
summary_writer = tf.summary.FileWriter(log_dir, sess.graph)
coord = tf.train.Coordinator()
tf.train.start_queue_runners(coord=coord, sess=sess)

# Training and validation loop:
with sess.as_default():
    epoch = 0
    while epoch < MAX_NUM_OF_EPOCHS:
        step = sess.run(global_step, feed_dict=None)
        epoch = step // EPOCH_SIZE
        batch_number = 0
        while batch_number < EPOCH_SIZE:
            start_time = time.time()
            feed_dict = {learning_rate_placeholder: LEARNING_RATE, phase_train_placeholder: True}
            total_err, reg_err, _, step, _ = sess.run([loss, reg_loss, train_op, global_step, summary_op],
                                                      feed_dict=feed_dict)
            duration = time.time() - start_time
            print('Epoch: %d batch: %d / %d \t time %.3f\t Loss %2.3f' %
                  (epoch, batch_number + 1, EPOCH_SIZE, duration, total_err))

            batch_number += 1

        print('Saving variables')
        start_time = time.time()
        checkpoint_path = os.path.join(model_dir, 'model.ckpt')
        saver.save(sess, checkpoint_path, global_step=step, write_meta_graph=False)
        save_time_variables = time.time() - start_time
        print('Variables saved in %.2f seconds' % save_time_variables)
        metagraph_filename = os.path.join(model_dir, 'model.meta')
        save_time_metagraph = 0
        if not os.path.exists(metagraph_filename):
            print('Saving metagraph')
            start_time = time.time()
            saver.export_meta_graph(metagraph_filename)
            save_time_metagraph = time.time() - start_time
            print('Metagraph saved in %.2f seconds' % save_time_metagraph)
        summary = tf.Summary()
        summary.value.add(tag='time/save_variables', simple_value=save_time_variables)
        summary.value.add(tag='time/save_metagraph', simple_value=save_time_metagraph)
        summary_writer.add_summary(summary, step)
