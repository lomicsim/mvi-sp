import math

import numpy as np
import tensorflow as tf

from Recognizer import Recognizer
from data import lfw
import utils

IMAGE_WIDTH = 112
IMAGE_HEIGHT = 112
EMBEDDING_SIZE = 112
MODEL_PATH = "model/trained_model"
IMG_FORMAT = "jpg"

TEST_BATCH_SIZE = 100

with tf.Session() as sess:

    recognizer = Recognizer(sess, MODEL_PATH, EMBEDDING_SIZE, IMAGE_WIDTH, IMAGE_HEIGHT)

    print('Testing LFW')
    pairs = lfw.read_pairs("dataset/lfw_pairs.txt")
    paths, ref = lfw.get_paths("dataset/lfw_aligned/", pairs, IMG_FORMAT)
    num_of_images = len(paths)
    num_of_batches = int(math.ceil(1.0 * num_of_images / TEST_BATCH_SIZE))
    emb_array = np.zeros((num_of_images, EMBEDDING_SIZE))
    for i in range(num_of_batches):
        start_index = i * TEST_BATCH_SIZE
        print('batch {}/{}'.format(start_index, num_of_images))
        end_index = min((i + 1) * TEST_BATCH_SIZE, num_of_images)
        paths_batch = paths[start_index:end_index]
        images = lfw.load_data(paths_batch, IMAGE_HEIGHT, IMAGE_WIDTH, (IMAGE_HEIGHT, IMAGE_WIDTH))
        feats = recognizer.get(images)
        emb_array[start_index:end_index, :] = feats

    tpr, fpr, accuracy = lfw.evaluate(emb_array, ref, num_of_folds=10)

    print('Accuracy: %1.3f' % np.mean(accuracy))
