import numpy as np
from sklearn.model_selection import KFold


def calculate_roc(thresholds, embeddings1, embeddings2, actual_same, num_of_folds=10):
    assert (embeddings1.shape[0] == embeddings2.shape[0])
    assert (embeddings1.shape[1] == embeddings2.shape[1])
    num_of_pairs = min(len(actual_same), embeddings1.shape[0])
    num_of_thresholds = len(thresholds)
    k_fold = KFold(n_splits=num_of_folds, shuffle=False)

    tprs = np.zeros((num_of_folds, num_of_thresholds))
    fprs = np.zeros((num_of_folds, num_of_thresholds))
    accuracy = np.zeros(num_of_folds)

    diff = np.subtract(embeddings1, embeddings2)
    dist = np.sum(np.square(diff), 1)
    indices = np.arange(num_of_pairs)
    for fold_idx, (train_set, test_set) in enumerate(k_fold.split(indices)):
        acc_train = np.zeros(num_of_thresholds)
        for threshold_idx, threshold in enumerate(thresholds):
            _, _, acc_train[threshold_idx] = calculate_accuracy(threshold, dist[train_set], actual_same[train_set])
        best_threshold_index = np.argmax(acc_train)
        for threshold_idx, threshold in enumerate(thresholds):
            tprs[fold_idx, threshold_idx], fprs[fold_idx, threshold_idx], _ = calculate_accuracy(threshold,
                                                                                                 dist[test_set],
                                                                                                 actual_same[
                                                                                                     test_set])
        _, _, accuracy[fold_idx] = calculate_accuracy(thresholds[best_threshold_index], dist[test_set],
                                                      actual_same[test_set])

    tpr = np.mean(tprs, 0)
    fpr = np.mean(fprs, 0)
    return tpr, fpr, accuracy


def calculate_accuracy(threshold, dist, actual_same):
    predict_same = np.less(dist, threshold)
    tp = np.sum(np.logical_and(predict_same, actual_same))
    fp = np.sum(np.logical_and(predict_same, np.logical_not(actual_same)))
    tn = np.sum(np.logical_and(np.logical_not(predict_same), np.logical_not(actual_same)))
    fn = np.sum(np.logical_and(np.logical_not(predict_same), actual_same))

    tpr = 0 if (tp + fn == 0) else float(tp) / float(tp + fn)
    fpr = 0 if (fp + tn == 0) else float(fp) / float(fp + tn)
    acc = float(tp + tn) / dist.size
    return tpr, fpr, acc


def l2_normalize(x):
    n, e = x.shape
    mean = np.mean(x, axis=1)
    mean = mean.reshape((n, 1))
    mean = np.repeat(mean, e, axis=1)
    x -= mean
    norm = np.linalg.norm(x, axis=1)
    norm = norm.reshape((n, 1))
    norm = np.repeat(norm, e, axis=1)
    y = np.multiply(x, 1 / norm)
    return y



