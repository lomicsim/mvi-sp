import numpy as np
from scipy import misc

import os
import utils


def get_paths(lfw_dir, pairs, file_ext):
    num_of_skipped_pairs = 0
    path_list = []
    same_list = []
    for pair in pairs:
        path0 = None
        same = True
        if len(pair) == 3:
            path0 = os.path.join(lfw_dir, pair[0], pair[0] + '_' + '%04d' % int(pair[1]) + '.' + file_ext)
            path1 = os.path.join(lfw_dir, pair[0], pair[0] + '_' + '%04d' % int(pair[2]) + '.' + file_ext)
            same = True
        elif len(pair) == 4:
            path0 = os.path.join(lfw_dir, pair[0], pair[0] + '_' + '%04d' % int(pair[1]) + '.' + file_ext)
            path1 = os.path.join(lfw_dir, pair[2], pair[2] + '_' + '%04d' % int(pair[3]) + '.' + file_ext)
            same = False
        if os.path.exists(path0) and os.path.exists(path1):  # Only add the pair if both paths exist
            path_list += (path0, path1)
            same_list.append(same)
        else:
            num_of_skipped_pairs += 1
    if num_of_skipped_pairs > 0:
        print('Skipped %d image pairs' % num_of_skipped_pairs)

    return path_list, same_list


def read_pairs(pairs_filename):
    pairs = []
    with open(pairs_filename, 'r') as f:
        for line in f.readlines()[1:]:
            pair = line.strip().split()
            pairs.append(pair)
    return np.array(pairs)


def to_rgb(img):
    w, h = img.shape
    ret = np.empty((w, h, 3), dtype=np.uint8)
    ret[:, :, 0] = ret[:, :, 1] = ret[:, :, 2] = img
    return ret


def load_data(image_paths, image_height, image_width, src_size=None):
    num_of_samples = len(image_paths)
    images = np.zeros((num_of_samples, image_height, image_width, 3))
    for i in range(num_of_samples):
        img = misc.imread(image_paths[i])
        if src_size is not None:
            img = misc.imresize(img, (src_size[0], src_size[1]))
        if img.ndim == 2:
            img = to_rgb(img)
        else:
            img = img - 127.5
            img = img / 128.
        images[i, :, :, :] = img
    return images


def evaluate(embeddings, actual_same, num_of_folds=10):
    thresholds = np.arange(0, 4, 0.01 / 4)
    embeddings1 = embeddings[0::2]
    embeddings2 = embeddings[1::2]
    tpr, fpr, accuracy = utils.calculate_roc(thresholds, embeddings1, embeddings2,
                                             np.asarray(actual_same), num_of_folds=num_of_folds)
    return tpr, fpr, accuracy
