import os.path
import numpy as np
import tensorflow as tf
import tensorflow.data as tf_data


DATA_DIR = "dataset/webface_aligned"
DATA_LIST = "dataset/webface_list.txt"
PERSONS_PER_BATCH = 20
IMAGES_PER_PERSON = 5
IMAGE_WIDTH = 112
IMAGE_HEIGHT = 112

global_ind = 0


def get_dataset(epoch_size, max_num_of_epochs):
    train_set = dataset_from_list(DATA_DIR, DATA_LIST)
    num_of_classes = len(train_set)
    print('num_of_classes: ', num_of_classes)
    image_list, label_list = get_image_paths_and_labels(train_set)
    print('total images: ', len(image_list))
    image_list = np.array(image_list)
    label_list = np.array(label_list, dtype=np.int32)

    dataset_size = len(image_list)
    single_batch_size = PERSONS_PER_BATCH * IMAGES_PER_PERSON
    indices = range(dataset_size)
    np.random.shuffle(indices)

    def _sample_people(_):
        global global_ind
        if global_ind >= dataset_size:
            np.random.shuffle(indices)
            global_ind = 0
        true_num_batch = min(single_batch_size, dataset_size - global_ind)

        sample_paths = image_list[indices[global_ind:global_ind + true_num_batch]]
        sample_labels = label_list[indices[global_ind:global_ind + true_num_batch]]

        global_ind += true_num_batch
        return np.array(sample_paths), np.array(sample_labels, dtype=np.int32)

    def _parse_function(filename, label):
        file_contents = tf.read_file(filename)
        image = tf.image.decode_image(file_contents, channels=3)
        image.set_shape((IMAGE_WIDTH, IMAGE_HEIGHT, 3))
        image = tf.cast(image, tf.float32)
        image = tf.subtract(image, 127.5)
        image = tf.div(image, 128.)
        return image, label

    def _from_tensor_slices(tensors_x, tensors_y):
        return tf_data.Dataset.from_tensor_slices((tensors_x, tensors_y))

    dataset = tf_data.Dataset.range(epoch_size * max_num_of_epochs * 100)
    dataset = dataset.map(lambda x: tf.py_func(_sample_people, [x], [tf.string, tf.int32]))
    dataset = dataset.flat_map(_from_tensor_slices)
    dataset = dataset.map(_parse_function, num_parallel_calls=2)
    dataset = dataset.batch(single_batch_size)
    it = dataset.make_initializable_iterator()
    next_it = it.get_next()
    next_it[0].set_shape((single_batch_size, IMAGE_HEIGHT, IMAGE_WIDTH, 3))
    next_it[1].set_shape(single_batch_size)

    return next_it[0], next_it[1], it, len(image_list)


class ImageClass:
    def __init__(self, name, image_paths):
        self.name = name
        self.image_paths = image_paths

    def __str__(self):
        return self.name + ', ' + str(len(self.image_paths)) + ' images'

    def __len__(self):
        return len(self.image_paths)


def dataset_from_list(data_dir, list_file):
    lines = open(list_file, 'r').read().strip().split('\n')
    path_exp = os.path.expanduser(data_dir)
    class_paths = {}
    for line in lines:
        image_path, _ = line.split(' ')
        class_name, _ = image_path.split('/')
        if class_name not in class_paths:
            class_paths[class_name] = []
        full_image_path = os.path.join(path_exp, image_path)
        if not os.path.exists(full_image_path):
            continue
        class_paths[class_name].append(full_image_path)
    dataset = []
    keys = class_paths.keys()
    keys.sort()
    for key in keys:
        dataset.append(ImageClass(key, class_paths[key]))
    return dataset


def get_image_paths_and_labels(dataset):
    image_paths_flat = []
    labels_flat = []
    for i in range(len(dataset)):
        image_paths_flat += dataset[i].image_paths
        labels_flat += [i] * len(dataset[i].image_paths)
    return image_paths_flat, labels_flat


