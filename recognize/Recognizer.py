import tensorflow as tf

import recognize.cnn
from recognize import utils


class Recognizer:

    def __init__(self, sess, model_path, embedding_size, image_width, image_height):
        print("loading model {} for feature extraction with embedding size = {}...".format(model_path, embedding_size))
        self.sess = sess
        self.image_width = image_width
        self.image_height = image_height
        self.images_placeholder = tf.placeholder(tf.float32, shape=(None, image_width, image_height, 3), name='image')
        self.phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')
        self.network = recognize.cnn.cnn(self.images_placeholder, embedding_size)
        self.embedding_size = self.network.get_shape()[1]

        saver = tf.train.Saver(tf.global_variables(), max_to_keep=3)
        saver.restore(sess, model_path)

    def get(self, images):
        feed_dict = {self.images_placeholder: images, self.phase_train_placeholder: False}
        feats = self.sess.run(self.network, feed_dict=feed_dict)
        return utils.l2_normalize(feats)

