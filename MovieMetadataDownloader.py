import os
import pickle
from os.path import join, exists

import requests
import time
import imdb


class Movie:
    def __init__(self, movie_obj, cast):
        self.movie_obj = movie_obj
        self.cast = cast


class Person:
    def __init__(self, headshot, name, id):
        self.headshot = headshot
        self.name = name
        self.id = id
        self.img = None
        self.embedding = None


def _download_headshot(location, img):
    headshot = requests.get(img).content
    with open(location, 'wb') as handler:
        handler.write(headshot)


class MovieMetadataDownloader:

    def __init__(self, args):
        self.ia = imdb.IMDb()
        self.person_dir = args.person_dir
        self.movie_dir = args.movie_dir
        self.max_movies_searched = args.max_movies_searched
        self.max_cast_size = args.max_cast_size
        if not os.path.exists(self.movie_dir):
            os.mkdir(self.movie_dir)
        if not os.path.exists(self.person_dir):
            os.mkdir(self.person_dir)

    def get_movie(self, name):
        """Downloads given movie's cast, their headshots
        returns set of pairs (personId, personName)."""

        searches = self.ia.search_movie(name, self.max_movies_searched)
        searches = list(filter(lambda a: a['kind'] == 'movie', searches))

        if len(searches) == 1:
            movieId = searches[0].movieID
        else:
            if len(searches) > 1:
                i = 1
                listOfIds = []
                print("Choose your movie:")
                for m in searches:
                    print("[%d] %s (%d)" % (i, m['title'], m['year']))
                    i += 1
                    listOfIds.append(m.movieID)
                print("[%d] quit" % i)
                num = int(input("Choose: "))
                if num >= i or num <= 0:
                    return
                else:
                    movieId = listOfIds[num - 1]
            else:
                return

        pickle_path = join(self.movie_dir, 'movie_{}.pickle'.format(movieId))
        if exists(pickle_path):
            return pickle.load(open(pickle_path, 'rb'))

        print("\t - downloading info about movie %s..." % name)
        movie = self.ia.get_movie(movieId, 'main')

        print("\t - downloading headshot images for %s..." % movie['title'])
        start = time.time()
        cast = [(Person(None, x['name'], x.personID), self.person_dir) for x in movie['cast'][0:self.max_cast_size]]
        ret = map(self._download_person, cast)
        print("\t -> downloading finished after %f s" % (time.time() - start))

        ret = Movie(movie, [x for x in ret if x.headshot is not None])
        pickle.dump(ret, open(pickle_path, 'wb'), protocol=2)
        return ret

    def _download_person(self, par):
        person, person_dir = par
        location = _file_location(person.id, person_dir)
        if not os.path.exists(location):
            person_detail = self.ia.get_person(person.id, 'main')
            if 'headshot' in person_detail:
                headshot = _download_headshot(location, person_detail['headshot'])
                person.headshot_location = location
                person.headshot = headshot
        else:
            person.headshot = location
        return person


def _file_location(person_id, person_dir):
    return "%s/%s.jpg" % (person_dir, person_id)
